#!/bin/bash

# django cannot deal with files with a byte order mark
has_bom() { head -c3 "$1" | grep -q $'\xef\xbb\xbf'; }

# get all po files, and check for bom
find . -name "*.po" | while read line
do
        if has_bom $line; then
                echo "Byte order mark found in $line!"
                exit 1
        fi
        output_dir=$(dirname $line)
        msgfmt --check-format -o "$output_dir/django.mo" $line
        if [ $? -ne 0 ]; then
                echo "$line failed to compile!"
                exit 1 
        fi 
done